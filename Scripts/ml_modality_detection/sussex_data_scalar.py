import os
import pandas as pd
from datetime import datetime
import pickle

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


random_state = 42
selected_features = ['distance_[m]_sum', 'speed_[m.s-1]_amin', 'speed_[m.s-1]_mean', 'speed_[m.s-1]_percentile_5',
                     'speed_[m.s-1]_percentile_85', 'acceleration_[m.s-2]_amin']
file_path = os.path.join('..', '..', '..', '..', '..', 'data', 'Sussex-Huawei Locomotion Dataset',
                         'training_data_2022-04-05.csv')

training_data = pd.read_csv(file_path).dropna()
X = training_data.copy()[selected_features]
y = training_data.copy()['mode_mode']

X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.2, stratify=y, random_state=random_state)

scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_valid)

# save the scaler
scalar_file_name = f"standard_scalar_{datetime.now().isoformat()}"
params_file_path = os.path.join('data_scalars', f"data_transformer_{scalar_file_name}.pkl")
pickle.dump(scaler, open(params_file_path, 'wb'))
