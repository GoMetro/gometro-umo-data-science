# import comet_ml in the top of your file
# import comet_ml
import os
import pandas as pd

# # Setting the API key (saved as environment variable)
# experiment = comet_ml.Experiment(
#     api_key=os.environ["COMET_API_KEY"],
#     project_name=os.environ["COMET_PROJECT_NAME"],
#     workspace=os.environ["COMET_WORKSPACE"]
# )

# example of param_grid searching key hyperparametres for logistic regression
from datetime import datetime
import pickle
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import f1_score, precision_score, recall_score, confusion_matrix, make_scorer

random_state = 42

# define dataset
# X, y = make_blobs(n_samples=1000, centers=2, n_features=100, cluster_std=20)
selected_features = ['distance_[m]_sum', 'speed_[m.s-1]_amin', 'speed_[m.s-1]_mean', 'speed_[m.s-1]_percentile_5',
                     'speed_[m.s-1]_percentile_85', 'acceleration_[m.s-2]_amin']
file_path = os.path.join('..', '..', '..', '..', '..', 'data', 'Sussex-Huawei Locomotion Dataset',
                         'training_data_2022-04-05.csv')

training_data = pd.read_csv(file_path).dropna()
X = training_data.copy()[selected_features]
y = training_data.copy()['mode_mode']

X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.2, stratify=y, random_state=random_state)

scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_valid)

# define models and parameters
model = KNeighborsClassifier()
leaf_size = range(1, 50)
n_neighbors = range(1, 30, 2)
weights = ['uniform', 'distance']
metric = ['euclidean', 'manhattan', 'minkowski']

# define grid search
param_grid = dict(leaf_size=leaf_size, n_neighbors=n_neighbors, weights=weights, metric=metric)

# define tuning strategy
score_metric = make_scorer(f1_score, average='weighted')
cv = RepeatedStratifiedKFold(n_splits=5, n_repeats=3, random_state=random_state)
clf = GridSearchCV(estimator=model, param_grid=param_grid, n_jobs=-1, cv=cv, scoring=score_metric, error_score=0)

# train
clf.fit(X_train_scaled, y_train)
y_pred = clf.predict(X_test_scaled)

print(f"\nResults\nConfusion matrix\n{confusion_matrix(y_valid, y_pred)}")

f1_weighted = f1_score(y_valid, y_pred, average='weighted')
precision_weighted = precision_score(y_valid, y_pred, average='weighted')
recall_weighted = recall_score(y_valid, y_pred, average='weighted')
f1_macro = f1_score(y_valid, y_pred, average='macro')
precision_macro = precision_score(y_valid, y_pred, average='macro')
recall_macro = recall_score(y_valid, y_pred, average='macro')
f1_micro = f1_score(y_valid, y_pred, average='micro')
precision_micro = precision_score(y_valid, y_pred, average='micro')
recall_micro = recall_score(y_valid, y_pred, average='micro')

data_set_columns = selected_features + ['mode_mode']
model_type = type(model).__name__
params = {"random_state": random_state,
          "model_type": model_type,
          "scaler": "standard scaler",
          "param_grid": str(param_grid),
          "stratify": True,
          "data_columns": '; '.join(data_set_columns)
          }

metrics = {"f1_weighted": f1_weighted,
           "recall_weighted": recall_weighted,
           "precision_weighted": precision_weighted,
           "f1_macro": f1_macro,
           "recall_macro": recall_macro,
           "precision_macro": precision_macro,
           "f1_micro": f1_micro,
           "recall_micro": recall_micro,
           "precision_micro": precision_micro
           }

print(metrics)
training_set = training_data[data_set_columns]

model_file_name = f"{model_type}_{datetime.now().isoformat()}"
params_file_path = os.path.join('model_parameters', f"model_params_{model_file_name}.csv")
eval_file_path = os.path.join('evaluation_metrics', f"model_evaluation_{model_file_name}.csv")
pkl_file_path = os.path.join('pkl_models', f"{model_file_name}.pkl")

df_params = pd.DataFrame({**{'model_version': model_file_name}, **params}, index=[0])
df_evaluation_metrics = pd.DataFrame({**{'model_version': model_file_name}, **metrics}, index=[0])

# save outputs
df_params.to_csv(params_file_path, index=False)
df_evaluation_metrics.to_csv(eval_file_path, index=False)
pickle.dump(model, open(pkl_file_path, 'wb'))

# print('logging experiment results...')
# experiment.log_dataset_hash(training_set.to_numpy())
# experiment.log_parameters(params)
# experiment.log_metrics(metrics)
# experiment.end()
# print('finished logging experiment results')
