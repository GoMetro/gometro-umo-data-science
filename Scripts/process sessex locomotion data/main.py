from datetime import date
import os
import pandas as pd

import process_functions

# get path for users
# folder structure
main_data_folder = os.path.join('..', '..', '..', '..', '..', 'data', 'Sussex-Huawei Locomotion Dataset')
subdir_l2_pref = 'SHLDataset_preview_v1_part'
subdir_l3 = 'SHLDataset_preview_v1'

user_1_root_dir = os.path.join(main_data_folder, f"{subdir_l2_pref}1", subdir_l3, 'User1')
user_2_root_dir = os.path.join(main_data_folder, f"{subdir_l2_pref}2", subdir_l3, 'User2')
user_3_root_dir = os.path.join(main_data_folder, f"{subdir_l2_pref}3", subdir_l3, 'User3')

user_1_record_ids = ['220617', '260617', '270617']
user_2_record_ids = ['140617', '140717', '180717']
user_3_record_ids = ['030717', '070717', '140617']

user_1_data_dirs = [os.path.join(user_1_root_dir, record_id) for record_id in user_1_record_ids]
user_2_data_dirs = [os.path.join(user_2_root_dir, record_id) for record_id in user_2_record_ids]
user_3_data_dirs = [os.path.join(user_3_root_dir, record_id) for record_id in user_3_record_ids]

# ensure that paths exist
all_raw_data_dirs = [i_path for path_list in [user_1_data_dirs, user_2_data_dirs, user_3_data_dirs] for i_path in
                     path_list]
for path in all_raw_data_dirs:
    print(path)

print(all([os.path.exists(i_path) for i_path in all_raw_data_dirs]))

body_positions = ['Hand', 'Bag', 'Hips', 'Torso']
file_suffixes = ['Location.txt']
raw_feature_file_names = [f"{position}_{file}" for position in body_positions for file in file_suffixes]
raw_label_file_name = 'labels_track_main.txt'

train_data_list = list()
# for each user survey day
for survey_day_path in all_raw_data_dirs:
    print(survey_day_path)

    # get labels_track_main.txt (main dataframe)
    file_path_labels = os.path.join(survey_day_path, raw_label_file_name)
    df_labels = process_functions.get_df_from_labels_track_main(file_path_labels)

    # for each raw feature file
    for feature_file_name in raw_feature_file_names:
        print(f"starting {feature_file_name}")
        file_path_features = os.path.join(survey_day_path, feature_file_name)
        df_features = process_functions.get_df_from_locations(file_path_features)

        # join features to labels
        df_features = process_functions.join_locations_to_labels(df_features, df_labels)

        # process features
        df_features = process_functions.prep_data_raw(df_features).dropna()

        # transform raw location data to leg observations
        df_legs = process_functions.summarise_locations_data(df_features)

        # add to training data
        train_data_list.append(df_legs)

        print(f"finished {feature_file_name}")

# consolidate training data
df_train = pd.concat(train_data_list)
print(df_train.info())
print(df_train.head())

# save training data
training_data_file_path = os.path.join(main_data_folder, f'training_data_{date.today()}.csv')
df_train.to_csv(training_data_file_path, index=False)
