import numpy as np
import pandas as pd

activity_label_dict = {
    0: 'still',
    1: 'still',
    2: 'still',
    3: 'still',
    4: 'walk',
    5: 'walk',
    6: 'run',
    7: 'bicycle',
    8: 'drive',
    10: 'bus',
    11: 'bus',
    12: 'bus',
    13: 'bus',
    14: 'train',
    15: 'train',
    16: 'subway',
    17: 'subway',
}


def get_df_from_labels_track_main(file_path):
    column_names = ['Label start time in millisecond', 'Label end time in millisecond', 'Activity label']
    df = pd.read_csv(file_path, delimiter=' ', names=column_names, index_col=False)
    df['mode'] = df['Activity label'].apply(lambda x: activity_label_dict[x])

    return df


def get_df_from_locations(file_path):
    locations_columns = ['Time [ms]', 'Ignore_1', 'Ignore_2', 'Accuracy [m]', 'Latitude [degrees]',
                         'Longitude [degrees]', 'Altitude [m]']
    column_names_to_use = [col for col in locations_columns
                           if ("Ignore" not in col)
                           and (col != 'Altitude [m]')]
    column_i_to_use = [locations_columns.index(col) for col in column_names_to_use]
    maximum_accuracy_m = 25
    df = pd.read_csv(file_path, delimiter=' ', usecols=column_i_to_use, names=column_names_to_use, index_col=False)
    df = df[df['Accuracy [m]'] <= maximum_accuracy_m].dropna()
    return df


def haversine_distance(lat1, lon1, lat2, lon2):
    """
    this function takes the coordinates between two points and returns
    the distance between the points of the given coordinates in meters.
    
    ------------------------------------
    Parameters:
        lat1 (float): latitude of first point
        lat2 (float): longitude of first point
        lon1 (float): latitude of second point
        lon2 (float): longitude of second point
    
    Returns: (float): distance between points (m)
    
    ------------------------------------
    source: https://towardsdatascience.com/heres-how-to-calculate-distance-between-2-geolocations-in-python-93ecab5bbba4
    
    ------------------------------------
    Example:
    
    

    """
    # define constants
    r = 6371

    # implement haversine formula
    phi1 = np.radians(lat1)
    phi2 = np.radians(lat2)
    delta_phi = np.radians(lat2 - lat1)
    delta_lambda = np.radians(lon2 - lon1)
    a = np.sin(delta_phi / 2) ** 2 + np.cos(phi1) * np.cos(phi2) * np.sin(delta_lambda / 2) ** 2
    res = r * (2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a)))

    # convert result to from km to m
    res = res * 1000

    return np.round(res, 2)


def prep_data_raw(df):
    """
    This function takes data unpacked from GoMetro's UMA REST API using the
    unpack_payload function and returns a dataframe with the following columns
    added to the data
    
    ------------------------------------
    Parameters:
        df (pandas.DataFrame): DataFrame containing column with payload
    
    Returns: (pandas.DataFrame): Dataframe with payload data in each column
    ------------------------------------
    Example:
    
    
    """

    # make copy of dataframe
    df = df.copy()

    # shift coordinates
    df['lat_shift'] = df['Latitude [degrees]'].shift(-1)
    df['lon_shift'] = df['Longitude [degrees]'].shift(-1)
    df['duration_[s]'] = df['Time [ms]'].diff() / 1000

    # get distance
    df['distance_[m]'] = df.apply(lambda x:
                                  haversine_distance(x['Latitude [degrees]'],
                                                     x['Longitude [degrees]'],
                                                     x['lat_shift'],
                                                     x['lon_shift']), axis=1)

    # add features
    df['speed_[m.s-1]'] = df['distance_[m]'] / df['duration_[s]']
    df['speed_change'] = df['speed_[m.s-1]'].diff()
    df['acceleration_[m.s-2]'] = df['speed_change'] / df['duration_[s]']

    df.drop(['speed_change', 'lat_shift', 'lon_shift'], axis=1, inplace=True)
    return df


def percentile(n):
    def percentile_(x):
        return x.quantile(n)

    percentile_.__name__ = f'percentile_{round(n * 100)}'
    return percentile_


# define aggregating functions
feat_distro = [np.min, np.max, np.mean, np.std,
               percentile(0.05), percentile(0.10), percentile(0.15), percentile(0.20), percentile(0.25),
               percentile(0.30), percentile(0.35), percentile(0.40), percentile(0.45), percentile(0.50),
               percentile(0.55), percentile(0.60), percentile(0.65), percentile(0.70), percentile(0.75),
               percentile(0.80), percentile(0.85), percentile(0.90), percentile(0.95)]

leg_agg_dict = {
    'distance_[m]': np.sum,
    'Activity label': pd.Series.mode,
    'mode': pd.Series.mode,
    'duration_[s]': np.sum,
    'speed_[m.s-1]': feat_distro,
    'acceleration_[m.s-2]': feat_distro,
}


def join_model_to_raw_trips(x, trip_table, join_cols, raw_time_col='Time [ms]',
                            trip_start_col='Label start time in millisecond',
                            trip_end_col='Label end time in millisecond'):
    """
    This function joins parsed trip reference data from GoMetro's UMA API to parsed
    raw trip data from GoMetro's API by using the apply method to raw trip data.
    
    ------------------------------------
    Parameters:
        x (pandas.Series):
        trip_table (pandas.DataFrame): 
        raw_time_col (str): 
        trip_start_col (str):
        trip_end_col (str):
        join_cols (list of str): 
    
    Returns:
        (pandas.Series): 
    
    ------------------------------------
    source: https://stackoverflow.com/questions/31513207/complicated-merge-based-on-start-and-end-date-pandas
    
    ------------------------------------
    Example:
    
    
    """

    # check legs that correspond to current timestamp
    mask = np.logical_and(trip_table[trip_start_col] <= x[raw_time_col],
                          trip_table[trip_end_col] >= x[raw_time_col])

    # get the required column values
    values = [trip_table.loc[mask][new_col].tolist()[0] for new_col
              in join_cols
              if len(trip_table.loc[mask][new_col].tolist()) > 0]

    return pd.Series(values)


def join_locations_to_labels(locations_df, label_df):
    locations_df = locations_df.copy()
    locations_df[label_df.columns] = locations_df.apply(
        lambda x: join_model_to_raw_trips(x, label_df, join_cols=label_df.columns), axis=1).dropna()
    return locations_df


def summarise_locations_data(df_raw):
    df_legs = df_raw.groupby(['Label start time in millisecond', 'Label end time in millisecond']).agg(
        leg_agg_dict).reset_index()
    df_legs.columns = ['_'.join(col).strip() if '' not in col else col[0] for col in df_legs.columns]
    return df_legs
