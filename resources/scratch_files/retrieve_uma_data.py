#
# # analysis
# import numpy as np
# import pandas as pd
# import json
#
# # other
# # import warnings
# import os
# import requests
#
# def raise_uma_response_exceptions(response):
#
#     """
#     This function raises exceptions for responses from the
#     UMA API
#
#     -----------------
#     Example:
#
#     """
#
#     if type(response) is not requests.models.Response:
#         raise TypeError("Expected type response.models.Response. instead passed {type(response)}")
#
#      # raise bad response error
#     if response.status_code is not 200:
#         raise ValueError('POST /tasks/ {}'.format(response.status_code))
#
#     # raise empty response error
#     assert len(response.json()) > 0, "response returned no data"
#
#     # raise response timeout error
#     if response.json() == {'message': 'Endpoint request timed out'}:
#         raise TimeoutError(response.json()['message'])
#
#
# class uma_authenticator():
#
#     """
#     An UMA token object is designed to store UMA authentication
#     credenitals and generate an UMA API authentication token
#
#     ------------
#     Example:
#
#     >>> from uma_api_authenticator import UmaApiAuthenticator
#     >>> from os.path import join
#     >>> auth_file_path = os.path.join('path/to/authentication/credentials/file.json')
#     >>> uma_auth = UmaApiAuthenticator(auth_file_path)
#     >>> uma_auth.generate_token()
#     >>> uma_api_token = uma_auth.token
#
#     """
#
#     endpoint = 'https://identity.gometroapp.com/auth/realms/platform/protocol/openid-connect/token'
#
#     def __init__(self, credentials_file_path):
#
#         self.credentials_file_path = credentials_file_path
#
#         with open(self.credentials_file_path , "r") as read_file:
#             self.credentials = json.load(read_file)
#
#     def fetch_request(self, endpoint):
#
#         self.response = requests.post(endpoint, data=self.credentials)
#         raise_uma_response_exceptions(self.response)
#
#     def generate_token(self):
#
#         self.fetch_request(uma_authenticator.endpoint)
#         self.token = self.response.json()["access_token"]
#
#
#
# class uma_api():
#
#     """
#     An UMA events object is designed to store request data for
#     calls made to the UMA API and retrieve the associated
#     response data
#
#     ------------
#     Example:
#     >>> endpoint =
#     >>> events_retriever = retrive_uma_events(endpoint)
#
#     """
#
#     def __init__(self, endpoint=None, events_table=None,
#     	uma_authenticator=None):
#
#         self.endpoint = endpoint
#         self.events_table = events_table
#         self.uma_authenticator = uma_authenticator
#
#     def get_participant_id(self):
#
#
#     def get_endpoint(self, endpoint):
#
#     	self.endpoint = endpoint
#
#     	return self
#
#
#     def fetch_request(self, uma_token):
#
#     	self.get_endpoint(endpoint)
#
#         headers = {'Authorization': f'Bearer {uma_token}'}
#
#         self.response = requests.get(self.endpoint, headers=headers)
#         raise_uma_response_exceptions(self.response)
#
#     def create_events_table(self, uma_token):
#
#         self.fetch_request(uma_token)
#         self.events_table = pd.DataFrame(self.response.json())
#
#