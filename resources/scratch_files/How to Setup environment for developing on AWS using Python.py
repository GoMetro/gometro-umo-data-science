import boto3

boto3.setup_default_session(profile_name='gmpd')
client = boto3.client('s3')

print(f"client is {client}")

res = client.list_buckets()

print(res)