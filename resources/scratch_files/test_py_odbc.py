# import pandas module
import pandas as pd
import psycopg2

conn = psycopg2.connect(host='postgresql.gometroapp.com', database='gometro_uma',
                        user='gometro_uma', password='gometro_uma')

cur = conn.cursor()

# execute a statement
print('PostgreSQL database version:')

cur.execute('SELECT version()')

db_version = cur.fetchone()

print(db_version)

cur.close()
conn.close()


# Create a connection to the database on the SQL server 
# import pyodbc
#
# print(pyodbc.drivers())
#
# # host = COMPUTERNAME\SQLEXPRESS
#
# uma_conn = pyodbc.connect("driver={CData ODBC Driver for PostgreSQL},  host='postgresql.gometroapp.com', database='gometro_uma', user='gometro_uma', password='gometro_uma")
#
# # Creating a pandas dataframe to store the data imported from SQL server:
# gdf_legs = pd.read_sql_query('select * from dbo.Actors', uma_conn)
#
# print(gdf_legs)
