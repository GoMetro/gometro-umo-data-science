from pyproj import Proj, Transformer as projection_transformer
from shapely.ops import transform as geometry_transformer


def reprojectGeometry(geometry, new_projection, original_projection=4326):

    project = projection_transformer.from_proj(
        Proj(init=f'epsg:{str(original_projection)}'),  # source coordinate system
        Proj(init=f'epsg:{str(new_projection)}'))  # destination coordinate system

    return geometry_transformer(project.transform, geometry)
