import trips
import indicator_results_builder
from pandas import set_option
set_option("display.max_columns", 50)

df_trips = trips.Trip().getTrips()

for trip in df_trips:



    indicator_results = indicator_results_builder.IndicatorResultsBuilder().setTrip(trip).getIndicatorResults()
    print(indicator_results.info())
    print(indicator_results.head(10))