from database import Database


class Trip:

    def __init__(self):
        self.trip_df = None

    def getTrips(self):
        database = Database()

        database.connect()

        table_query = "SELECT A.id AS leg_id, F.participant_id, A.polyline," \
                      "B.departure_time AS leg_start_timestamp, " \
                      "C.arrival_time AS leg_end_timestamp, " \
                      "st_x(st_astext(D.location, 4326)) as leg_start_lat, " \
                      "st_y(st_astext(D.location, 4326)) as leg_start_long, " \
                      "st_x(st_astext(E.location, 4326)) as leg_end_lat, " \
                      "st_y(st_astext(E.location, 4326)) as leg_end_long, " \
                      "A.trip_id, " \
                      "F.departure_time AS trip_start_timestamp, F.arrival_time AS trip_end_timestamp " \
                      "FROM uma_participant_trip_leg AS A " \
                      "INNER JOIN uma_participant_stopover AS B ON A.from_stopover_id = B.id " \
                      "INNER JOIN uma_participant_stopover AS C ON A.to_stopover_id = C.id " \
                      "INNER JOIN uma_participant_place AS D ON B.place_id = D.id " \
                      "INNER JOIN uma_participant_place AS E ON C.place_id = E.id " \
                      "INNER JOIN uma_participant_trip AS F ON A.trip_id = F.id " \
                      "limit 500"

        self.trip_df = database.query_to_df(table_query)

        return self.trip_df


#
# [
#         'participant' => '123-456-789'
# 'trips' => [
#         [
#                 'trip_id => '987-654-321''
#             'legs' => [
#         [
#
#
#         ]
#
# ]
#
#         ]
#
# ]
#     ]
# ]
#
#         return results

# df = Trip().getTrips()
# df['trip_duration'] = (df['arrival_time'] - df['departure_time']).dt.seconds
#
# print(df.info())
# print(df['trip_duration'].head())
