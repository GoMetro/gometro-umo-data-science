from ..db import trips

df = trips.Trip().getTrips()
df['trip_duration'] = (df['arrival_time'] - df['departure_time']).dt.seconds

print(df.info())
print(df['trip_duration'].head())