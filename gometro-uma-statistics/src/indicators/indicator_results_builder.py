import trips
from geopandas import GeoDataFrame, read_file, points_from_xy
from os import path
from pyproj import Proj, CRS, Transformer as projection_transformer
from shapely.ops import transform as geometry_transformer
from shapely.geometry import Point, LineString
from pypolyline.util import decode_polyline

class IndicatorResultsBuilder:

    def __init__(self):

        self.indicator_columns = ['leg_id', 'participant_id', 'trip_id', 'trip_start_timestamp', 'trip_end_timestamp']
        self.df_indicator_results = None

    def setTrip(self, trip):

        self.trip = trip


    def addFieldToIndicators(self, field, field_position):

        self.indicator_columns.insert(field_position, field)

        return self.indicator_columns

    def getLegDurationIndicator(self):

        self.df_trips['leg_duration'] = (self.df_trips['leg_end_timestamp'] - self.df_trips['leg_start_timestamp'])\
            .dt.seconds

        self.addFieldToIndicators('leg_duration', 2)

        return self.df_trips

    def getPointsFromOdCoords(self):

        # prepare geometry data
        legstart_long = self.df_trips['leg_start_long']
        legstart_lat = self.df_trips['leg_start_lat']
        legend_long = self.df_trips['leg_end_long']
        legend_lat = self.df_trips['leg_end_lat']

        legstart_points = points_from_xy(legstart_long, legstart_lat, crs='EPSG:4326')
        legend_points = points_from_xy(legend_long, legend_lat, crs='EPSG:4326')

        # create geodataframe
        self.df_trips = GeoDataFrame(self.df_trips, geometry=legstart_points).\
            rename(columns={'geometry': 'legstart_location'})
        self.df_trips = GeoDataFrame(self.df_trips, geometry=legend_points).\
            rename(columns={'geometry': 'legend_location'})

        return self.df_trips

    def getPolylineGeometryFromPolylineCode(self):

        print(self.df_trips['polyline'].head(10))

        self.df_trips['leg_geometry'] = self.df_trips['polyline'].\
            apply(lambda x: [Point(xy) for xy in decode_polyline(bytes(x, encoding='utf-8'), 5)])

        self.df_trips['leg_geometry'] = self.df_trips['leg_geometry'].apply(LineString)

        return self.df_trips

    def changePolylineProjection(self):


        pass

    def getLegDistanceIndicator(self):

        pass

    def getIndicatorResults(self):

        self.getLegDurationIndicator()
        self.getPointsFromOdCoords()
        # self.getPolylineGeometryFromPolylineCode()
        # self.getLegDistanceIndicator()

        self.df_indicator_results = self.df_trips.copy()[self.indicator_columns]

        return self.df_indicator_results
