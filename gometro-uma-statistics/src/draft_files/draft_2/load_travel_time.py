import helper_functions
from geopandas import GeoDataFrame, read_file, points_from_xy
from os import path

# Connection parameters, yours will be different
uma_param_dic = {
    "host": "postgresql.gometroapp.com",
    "database": "gometro_uma",
    "user": "gometro_uma",
    "password": "gometro_uma"
}

# Connect to the database
uma_conn = helper_functions.connect(uma_param_dic)

column_names = ["leg_id",
                "participant_id",
                "polyline",
                "legstart_timestamp",
                "legstart_lat",
                "legstart_long",
                "legend_timestamp",
                "legend_lat",
                "legend_long",
                ]

table_query = ' '.join(["SELECT",
                        "A.id AS leg_id,",
                        "F.participant_id,",
                        "A.polyline,",
                        "B.departure_time AS legstart_timestamp,",
                        "st_x(st_astext(D.location, 4326)) as legstart_lat,",
                        "st_y(st_astext(D.location, 4326)) as legstart_long,",
                        "C.arrival_time AS legend_timestamp,",
                        "st_x(st_astext(E.location, 4326)) as legend_lat,",
                        "st_y(st_astext(E.location, 4326)) as legend_long",
                        "FROM uma_participant_trip_leg AS A",
                        "INNER JOIN uma_participant_stopover AS B ON A.from_stopover_id = B.id",
                        "INNER JOIN uma_participant_stopover AS C ON A.to_stopover_id = C.id",
                        "INNER JOIN uma_participant_place AS D ON B.place_id = D.id",
                        "INNER JOIN uma_participant_place AS E ON C.place_id = E.id",
                        "INNER JOIN uma_participant_trip AS F ON A.trip_id = F.id",
                        "WHERE F.participant_id IN",
                        "('3465a460-2d33-493c-99ba-2166b14ca898', '36e40431-212c-4323-bf58-9edb7a4a2813', ",
                        "'53714cf9-b990-476e-95a4-3201c113c6b9', '8711bc2f-4412-4bc2-9785-e8d6bed45ef7', ",
                        "'957f287b-2467-4ec0-b3bf-c9627b4b2230', '99fed303-77bb-417b-89b5-b70a87bf4a49', ",
                        "'abe17649-d025-45c9-96b8-a7468e8cf4a2', 'd4826376-8c0e-48b2-8e31-ad2edc44d1f4', ",
                        "'f5477b8e-26bb-4e21-9eb1-6d75cbcaa6ce', 'fa0ce243-b8e9-4267-a465-18d8902ca19c') ",
                        "limit 100"])

# create dataframe
gdf_legs = helper_functions.fetch_postgres_to_dataframe(uma_conn, table_query, column_names)

# prepare geometry data
legstart_long = gdf_legs['legstart_long']
legstart_lat = gdf_legs['legstart_lat']
legend_long = gdf_legs['legend_long']
legend_lat = gdf_legs['legend_lat']

legstart_points = points_from_xy(legstart_long, legstart_lat, crs='EPSG:4326')
legend_points = points_from_xy(legend_long, legend_lat, crs='EPSG:4326')

# create geodataframe
gdf_legs = GeoDataFrame(gdf_legs, geometry=legstart_points).rename(columns={'geometry': 'legstart_location'})
gdf_legs = GeoDataFrame(gdf_legs, geometry=legend_points).rename(columns={'geometry': 'legend_location'})

# assess geodataframe
print(gdf_legs.info())
print(gdf_legs.crs)
print(gdf_legs[["legstart_location",
                "legend_location"]].head())

# calculate indicators
gdf_legs['trip_duration'] = (gdf_legs['legend_timestamp'] - gdf_legs['legstart_timestamp']).dt.seconds

print("Import GeoJson")

file_path = path.join('..', '..', '..', '..', 'resources', 'data', 'external', 'places_spatial_data',
                      'latam_geojson', 'buenos-aires.geojson')

print(f"file path: {file_path}")
print(path.exists(file_path))

gdf_places = read_file(file_path)
print(gdf_places.info())
print(gdf_places.crs)
print(gdf_places['geometry'])

print("spatial join goejson to database")

indicator_columns = ['participant_id',
                     'mo_city_code',
                     'mo_taz_code',
                     'trip_duration']

gdf_legs_with_places = helper_functions.spatial_join(gdf_legs, gdf_places, how='inner', op='intersects',
                                                     left_col='legstart_location')
gdf_legs_with_places = helper_functions.spatial_join(gdf_legs, gdf_places, how='inner', op='intersects',
                                                     left_col='legend_location')
gdf_legs_with_places = gdf_legs_with_places[indicator_columns]

print('aggregate indicators')

df_indicator_agg = gdf_legs_with_places.groupby(['participant_id',
                                                 'mo_city_code',
                                                 'mo_taz_code']).sum()
df_indicator_agg.reset_index(inplace=True)

print(df_indicator_agg.info())

print('load indicators to database')

indicators_param_dic = {
    "host": "localhost",
    "database": "umatest",
    "user": "postgres",
    "password": "QSEpJehN9fV6"
}

indicators_conn = helper_functions.connect(indicators_param_dic)
helper_functions.insert_dataframe_to_postgres_bulk(indicators_conn,df_indicator_agg,
                                                   'participant_traveltime_per_city_zone')

print('successfully loaded')