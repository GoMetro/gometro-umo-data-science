import pandas as pd
import geopandas as gpd
import psycopg2
import sys
from io import StringIO
from pyproj import CRS, Transformer
from shapely.ops import transform
from shapely.geometry import Point, LineString
from pypolyline.util import decode_polyline



def connect(params_dic):
    """ Connect to the PostgreSQL database server """

    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params_dic)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
        sys.exit(1)

    print("Connection successful")

    return conn


def fetch_postgres_to_dataframe(conn, select_query, column_names):
    """
    Tranform a SELECT query into a pandas dataframe
    """
    cursor = conn.cursor()
    try:
        cursor.execute(select_query)
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cursor.close()
        return 1

    # Naturally we get a list of tupples
    tupples = cursor.fetchall()
    cursor.close()

    # We just need to turn it into a pandas dataframe
    df = pd.DataFrame(tupples, columns=column_names)
    return df


def insert_dataframe_to_postgres_bulk(conn, df, table):

    """
    Here we are going save the dataframe in memory
    and use copy_from() to copy it to the table
    """

    # save dataframe to an in memory buffer
    buffer = StringIO()
    df.to_csv(buffer, index_label='id', header=False)
    buffer.seek(0)

    cursor = conn.cursor()
    try:
        cursor.copy_from(buffer, table, sep=",")
        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        conn.rollback()
        cursor.close()
        return 1
    print("copy_from_stringio() done")
    cursor.close()

def calc_polyline_distance(polyline, local_survey_epsg):

    wgs84_proj = CRS('EPSG:4326')
    distance_proj = CRS(f"EPSG:{str(local_survey_epsg)}")
    proj_transform = Transformer.from_crs(wgs84_proj, distance_proj, always_xy=True).transform

    polyline_point_list = [Point(xy) for xy in decode_polyline(bytes(polyline, encoding='utf-8'), 5)]
    polyline_point_list = [transform(proj_transform, i) for i in polyline_point_list]

    return LineString(polyline_point_list).length


def spatial_join(left_df, right_df, how, op, left_col=None, right_col=None):
    left_df = left_df.copy()
    right_df = right_df.copy()

    if left_col is not None:
        left_df = left_df.rename(columns={left_col: 'geometry'})

    if right_col is not None:
        right_df = right_df.rename(columns={right_col: 'geometry'})

    return gpd.sjoin(left_df, right_df, how=how, op=op)
