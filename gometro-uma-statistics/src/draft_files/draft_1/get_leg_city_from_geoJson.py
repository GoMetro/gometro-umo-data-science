import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import geopandas as gpd

from urllib import request
from geotext import GeoText
import psycopg2
import sys
import os

# from geopy.geocoders import Nominatim
# from geopy.exc import GeocoderTimedOut
#
# from shapely.geometry import Point, Polygon
# import descartes

# Connection parameters, yours will be different
param_dic = {
    "host": "postgresql.gometroapp.com",
    "database": "gometro_uma",
    "user": "gometro_uma",
    "password": "gometro_uma"
}


def connect(params_dic):
    """ Connect to the PostgreSQL database server """

    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params_dic)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

        sys.exit(1)
    print("Connection successful")

    return conn


def postgresql_to_dataframe(conn, select_query, column_names):
    """
    Tranform a SELECT query into a pandas dataframe
    """
    cursor = conn.cursor()
    try:
        cursor.execute(select_query)
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cursor.close()
        return 1

    # Naturally we get a list of tupples
    tupples = cursor.fetchall()
    cursor.close()

    # We just need to turn it into a pandas dataframe
    df = pd.DataFrame(tupples, columns=column_names)
    return df


print('import from database')

# Connect to the database
conn = connect(param_dic)

column_names = ["leg_id",
                "participant_id",
                "legstart_timestamp",
                "legstart_location",
                "legend_timestamp",
                "legend_location"
                ]

# table_query = ' '.join(["SELECT",
#                         "id AS place_id,",
#                         "participant_id,",
#                         "address",
#                         "FROM uma_participant_place",
#                         "WHERE participant_id IN",
#                         "('3465a460-2d33-493c-99ba-2166b14ca898', '36e40431-212c-4323-bf58-9edb7a4a2813', ",
#                         "'53714cf9-b990-476e-95a4-3201c113c6b9', '8711bc2f-4412-4bc2-9785-e8d6bed45ef7', ",
#                         "'957f287b-2467-4ec0-b3bf-c9627b4b2230', '99fed303-77bb-417b-89b5-b70a87bf4a49', ",
#                         "'abe17649-d025-45c9-96b8-a7468e8cf4a2', 'd4826376-8c0e-48b2-8e31-ad2edc44d1f4', ",
#                         "'f5477b8e-26bb-4e21-9eb1-6d75cbcaa6ce', 'fa0ce243-b8e9-4267-a465-18d8902ca19c') ",
#                         "limit 100"])

table_query = ' '.join(["SELECT",
                        "A.id AS leg_id,",
                        "F.participant_id,",
                        "B.departure_time AS legstart_timestamp,",
                        "D.location AS legstart_location,",
                        "C.arrival_time AS legend_timestamp,",
                        "E.location AS legend_location",
                        "FROM uma_participant_trip_leg AS A",
                        "INNER JOIN uma_participant_stopover AS B ON A.from_stopover_id = B.id",
                        "INNER JOIN uma_participant_stopover AS C ON A.to_stopover_id = C.id",
                        "INNER JOIN uma_participant_place AS D ON B.place_id = D.id",
                        "INNER JOIN uma_participant_place AS E ON C.place_id = E.id",
                        "INNER JOIN uma_participant_trip AS F ON A.trip_id = F.id",
                        "WHERE F.participant_id IN",
                        "('3465a460-2d33-493c-99ba-2166b14ca898', '36e40431-212c-4323-bf58-9edb7a4a2813', ",
                        "'53714cf9-b990-476e-95a4-3201c113c6b9', '8711bc2f-4412-4bc2-9785-e8d6bed45ef7', ",
                        "'957f287b-2467-4ec0-b3bf-c9627b4b2230', '99fed303-77bb-417b-89b5-b70a87bf4a49', ",
                        "'abe17649-d025-45c9-96b8-a7468e8cf4a2', 'd4826376-8c0e-48b2-8e31-ad2edc44d1f4', ",
                        "'f5477b8e-26bb-4e21-9eb1-6d75cbcaa6ce', 'fa0ce243-b8e9-4267-a465-18d8902ca19c') ",
                        "limit 100"])

# Execute the "SELECT *" query
df_legs = postgresql_to_dataframe(conn, table_query, column_names)

print(df_legs)
print(df_legs.info())
print(df_legs[["legstart_location", "legend_location"]].head())
print(df_legs["legstart_location"][0])

print("\n")

# Close the connection
conn.close()

print("Import GeoJson")

file_path = os.path.join('../..', '..', '..', 'resources', 'data', 'external', 'places_spatial_data',
                         'latam_geojson', 'buenos-aires.geojson')

print(f"file path: {file_path}")
print(os.path.exists(file_path))

gdf_taz = gpd.read_file(file_path, )
print(gdf_taz.info())
print(gdf_taz.crs)

# # create point features
# lon = temp_df['stop_lat']
# lat = temp_df['stop_lon']
# temp_gdf = gpd.GeoDataFrame(temp_df,
#                             geometry=gpd.points_from_xy(lon, lat),
#                             crs={'init': 'epsg:4326'})
