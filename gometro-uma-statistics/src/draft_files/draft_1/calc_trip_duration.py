import psycopg2
import sys
import pandas as pd

# Connection parameters, yours will be different
param_dic = {
    "host": "postgresql.gometroapp.com",
    "database": "gometro_uma",
    "user": "gometro_uma",
    "password": "gometro_uma"
}


def connect(params_dic):
    """ Connect to the PostgreSQL database server """

    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params_dic)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

        sys.exit(1)
    print("Connection successful")

    return conn


def postgresql_to_dataframe(conn, select_query, column_names):
    """
    Tranform a SELECT query into a pandas dataframe
    """
    cursor = conn.cursor()
    try:
        cursor.execute(select_query)
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cursor.close()
        return 1

    # Naturally we get a list of tupples
    tupples = cursor.fetchall()
    cursor.close()

    # We just need to turn it into a pandas dataframe
    df = pd.DataFrame(tupples, columns=column_names)
    return df


print('Demo 1: keeping the original column names')

# Connect to the database
conn = connect(param_dic)

column_names = ["id",
                "arrival_time",
                "departure_time",
                "participant_id",
                "created_by",
                "created_date",
                "last_modified_by",
                "last_modified_date",
                "deleted",
                "deleted_by",
                "deleted_date"]


table_query = ' '.join(["select *",
                        "from uma_participant_trip",
                        "limit 1000"])

# Execute the "SELECT *" query
df = postgresql_to_dataframe(conn, table_query, column_names)
print(df.info())
print("\n")

df['trip_duration'] = (df['arrival_time'] - df['departure_time']).dt.seconds

print(df.info())
print(df['trip_duration'].head())


# Close the connection
conn.close()
