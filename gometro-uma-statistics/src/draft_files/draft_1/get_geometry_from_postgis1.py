from sqlalchemy import create_engine
import geopandas as gpd
import os

# visualisation libraries
from matplotlib import pyplot as plt
import seaborn as sns
sns.set(style='whitegrid', palette='muted',
        rc={'figure.figsize': (15,10)})


# Connection parameters, yours will be different
param_dic = {
    "host": "postgresql.gometroapp.com",
    "database": "gometro_uma",
    "user": "gometro_uma",
    "password": "gometro_uma"
}

# db_connection_url = f"postgres://{uma_param_dic['user']}:{uma_param_dic['password']}@{uma_param_dic['host']}:5432/{uma_param_dic['database']}"

def create_engine_from_params(param_dic):

    user = param_dic['user']
    password = param_dic['password']
    host = param_dic['host']
    database = param_dic['database']

    db_connection_url = f"postgres://{user}:{password}@{host}:5432/{database}"

    db_engine = create_engine(db_connection_url)

    print("successful connection.\n")
    print(f"host: {host}\ndatabase: {database}")

    return db_engine

print("import database")

table_query = ' '.join(["SELECT",
                        "A.id AS leg_id,",
                        "F.participant_id,",
                        "B.departure_time AS legstart_timestamp,",
                        "D.location AS legstart_location,",
                        "C.arrival_time AS legend_timestamp,",
                        "E.location AS legend_location",
                        "FROM uma_participant_trip_leg AS A",
                        "INNER JOIN uma_participant_stopover AS B ON A.from_stopover_id = B.id",
                        "INNER JOIN uma_participant_stopover AS C ON A.to_stopover_id = C.id",
                        "INNER JOIN uma_participant_place AS D ON B.place_id = D.id",
                        "INNER JOIN uma_participant_place AS E ON C.place_id = E.id",
                        "INNER JOIN uma_participant_trip AS F ON A.trip_id = F.id",
                        "WHERE F.participant_id IN",
                        "('3465a460-2d33-493c-99ba-2166b14ca898', '36e40431-212c-4323-bf58-9edb7a4a2813', ",
                        "'53714cf9-b990-476e-95a4-3201c113c6b9', '8711bc2f-4412-4bc2-9785-e8d6bed45ef7', ",
                        "'957f287b-2467-4ec0-b3bf-c9627b4b2230', '99fed303-77bb-417b-89b5-b70a87bf4a49', ",
                        "'abe17649-d025-45c9-96b8-a7468e8cf4a2', 'd4826376-8c0e-48b2-8e31-ad2edc44d1f4', ",
                        "'f5477b8e-26bb-4e21-9eb1-6d75cbcaa6ce', 'fa0ce243-b8e9-4267-a465-18d8902ca19c') ",
                        "limit 100"])

# connection = create_engine(db_connection_url)

connection = create_engine_from_params(param_dic)

gdf_legs = gpd.GeoDataFrame.from_postgis(table_query, connection, geom_col='legstart_location')

print(gdf_legs)
print(gdf_legs.info())
print(gdf_legs.crs)
print(gdf_legs[["legstart_location", "legend_location"]].head())
print(gdf_legs["legstart_location"][0])


print("Import GeoJson")

file_path = os.path.join('../..', '..', '..', 'resources', 'data', 'external', 'places_spatial_data',
                         'latam_geojson', 'buenos-aires.geojson')

print(f"file path: {file_path}")
print(os.path.exists(file_path))

gdf_taz = gpd.read_file(file_path, )
print(gdf_taz.info())
print(gdf_taz.crs)

print("spatial join goejson to database")

legs_with_places = gpd.sjoin(gdf_legs, gdf_taz, how='inner', op='touches')
print(legs_with_places.info())
print(legs_with_places.crs)

aa = gdf_taz.plot()
gdf_legs.plot(ax=aa)
plt.show()