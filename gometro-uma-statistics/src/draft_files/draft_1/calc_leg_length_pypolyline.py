import psycopg2
import sys
import pandas as pd
from pypolyline.util import encode_coordinates, decode_polyline
from shapely.geometry import Point, LineString, shape
from shapely.ops import transform
import pyproj

# Connection parameters, yours will be different
param_dic = {
    "host": "postgresql.gometroapp.com",
    "database": "gometro_uma",
    "user": "gometro_uma",
    "password": "gometro_uma"
}


def connect(params_dic):
    """ Connect to the PostgreSQL database server """

    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params_dic)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

        sys.exit(1)
    print("Connection successful")

    return conn


def postgresql_to_dataframe(conn, select_query, column_names):
    """
    Tranform a SELECT query into a pandas dataframe
    """
    cursor = conn.cursor()
    try:
        cursor.execute(select_query)
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cursor.close()
        return 1

    # Naturally we get a list of tupples
    tupples = cursor.fetchall()
    cursor.close()

    # We just need to turn it into a pandas dataframe
    df = pd.DataFrame(tupples, columns=column_names)
    return df


print('Demo 1: keeping the original column names')

# Connect to the database
conn = connect(param_dic)

column_names = ["id",
                "from_stopover_id",
                "mode",
                "polyline",
                "to_stopover_id",
                "trip_id"]

table_query = ' '.join(["select *",
                        "from uma_participant_trip_leg",
                        "limit 10"])

# Execute the "SELECT *" query
df = postgresql_to_dataframe(conn, table_query, column_names)
print(df.info())
print("\n")

# coords = [
#     [52.64125, 23.70162],
#     [52.64938, 23.70154],
#     [52.64957, 23.68546],
#     [52.64122, 23.68549],
#     [52.64125, 23.70162]
# ]

# point_list = [Point(xy) for xy in coords]
# print(f"point_list:\n{point_list}")

wgs84 = pyproj.CRS('EPSG:4326')
engineering_proj = pyproj.CRS('EPSG:31983')
engineering_proj_transform = pyproj.Transformer.from_crs(wgs84, engineering_proj, always_xy=True).transform
# engineering_proj_point_list = [transform(engineering_proj_transform, i) for i in point_list]
# print(f"projected_point_list:\n{engineering_proj_point_list}")

# polyline_geometry = LineString(point_list)
# print(f"polyline_geometry:\n{polyline_geometry}")

# projected_polyline_geometry = LineString(engineering_proj_point_list)
# print(f"projected_polyline_geometry:\n{projected_polyline_geometry}")

# print(f"polyline length:\n{projected_polyline_geometry.length}")

# precision is 5 for Google Polyline, 6 for OSRM / Valhalla
# polyline = encode_coordinates(coords, 5)
# print(polyline)

def calc_polyline_distance(polyline, local_survey_epsg):

    wgs84 = pyproj.CRS('EPSG:4326')
    distance_proj = pyproj.CRS(f"EPSG:{str(local_survey_epsg)}")
    transformation = pyproj.Transformer.from_crs(wgs84, distance_proj, always_xy=True).transform

    # polyline_point_list = decode_polyline(bytes(polyline, encoding='utf-8'))
    polyline_point_list = [Point(xy) for xy in decode_polyline(bytes(polyline, encoding='utf-8'), 5)]
    polyline_point_list = [transform(transformation, i) for i in polyline_point_list]

    return LineString(polyline_point_list).length





print(decode_polyline(b'ynh`IcftoCyq@Ne@ncBds@EEycB', 5))
print(decode_polyline(bytes('ynh`IcftoCyq@Ne@ncBds@EEycB', encoding='utf-8'), 5))

# df['polyline_geometry'] = df['polyline'].apply(lambda x: decode_polyline(bytes(x, encoding='utf-8'), 5))
#
# df['polyline_geometry'] = df['polyline_geometry'].apply(lambda coords:  [Point(xy) for xy in coords])
# df['polyline_geometry'] = df['polyline_geometry'].apply(lambda x: [transform(engineering_proj_transform, i)
#                                                                    for i in x])
# df['polyline_geometry'] = df['polyline_geometry'].apply(LineString)
# df['leg_length'] = df['polyline_geometry'].apply(lambda x: x.length)

df['leg_length'] = df['polyline'].apply(lambda x: calc_polyline_distance(x, 31983))

# LineString([(2, 0), (2, 4), (3, 4)])

print(df.info())
print(df['leg_length'].head())

length_test_line = df['leg_length'][0]

print('length_test_line:\n')
print(length_test_line)

# Close the connection
conn.close()
