import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import geopandas as gpd

from urllib import request
from geotext import GeoText
import psycopg2
import sys

# from geopy.geocoders import Nominatim
# from geopy.exc import GeocoderTimedOut
#
# from shapely.geometry import Point, Polygon
# import descartes

# Connection parameters, yours will be different
param_dic = {
    "host": "postgresql.gometroapp.com",
    "database": "gometro_uma",
    "user": "gometro_uma",
    "password": "gometro_uma"
}


def connect(params_dic):
    """ Connect to the PostgreSQL database server """

    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params_dic)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

        sys.exit(1)
    print("Connection successful")

    return conn


def postgresql_to_dataframe(conn, select_query, column_names):
    """
    Tranform a SELECT query into a pandas dataframe
    """
    cursor = conn.cursor()
    try:
        cursor.execute(select_query)
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cursor.close()
        return 1

    # Naturally we get a list of tupples
    tupples = cursor.fetchall()
    cursor.close()

    # We just need to turn it into a pandas dataframe
    df = pd.DataFrame(tupples, columns=column_names)
    return df


print('Demo 1: keeping the original column names')

# Connect to the database
conn = connect(param_dic)

column_names = ["leg_id",
                "participant_id",
                "address"
                ]


table_query = ' '.join(["SELECT",
                        "id AS place_id,",
                        "participant_id,",
                        "address",
                        "FROM uma_participant_place",
                        "WHERE participant_id IN",
                        "('3465a460-2d33-493c-99ba-2166b14ca898', '36e40431-212c-4323-bf58-9edb7a4a2813', "
                        "'53714cf9-b990-476e-95a4-3201c113c6b9', '8711bc2f-4412-4bc2-9785-e8d6bed45ef7', "
                        "'957f287b-2467-4ec0-b3bf-c9627b4b2230', '99fed303-77bb-417b-89b5-b70a87bf4a49', "
                        "'abe17649-d025-45c9-96b8-a7468e8cf4a2', 'd4826376-8c0e-48b2-8e31-ad2edc44d1f4', "
                        "'f5477b8e-26bb-4e21-9eb1-6d75cbcaa6ce', 'fa0ce243-b8e9-4267-a465-18d8902ca19c') "
                        "limit 100"])

# Execute the "SELECT *" query
df = postgresql_to_dataframe(conn, table_query, column_names)

print(df.info())
print(df['address'].head())

def get_cities_from_text(raw_text):

    places = GeoText(raw_text)

    return list(places.cities)

df['cities'] = df['address'].apply(get_cities_from_text)

print(df['cities'].head())

print("\n")

