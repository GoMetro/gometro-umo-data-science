from sqlalchemy import create_engine
import psycopg2
import sys
import geopandas as gpd
import pandas as pd
import os

# visualisation libraries
from matplotlib import pyplot as plt
import seaborn as sns

sns.set(style='whitegrid', palette='muted',
        rc={'figure.figsize': (15, 10)})

# Connection parameters, yours will be different
param_dic = {
    "host": "postgresql.gometroapp.com",
    "database": "gometro_uma",
    "user": "gometro_uma",
    "password": "gometro_uma"
}


# db_connection_url = f"postgres://{uma_param_dic['user']}:{uma_param_dic['password']}@{uma_param_dic['host']}:5432/{uma_param_dic['database']}"

def connect(params_dic):
    """ Connect to the PostgreSQL database server """

    conn = None
    try:
        # connect to the PostgreSQL server
        print('Connecting to the PostgreSQL database...')
        conn = psycopg2.connect(**params_dic)

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)

        sys.exit(1)
    print("Connection successful")

    return conn


def postgresql_to_dataframe(conn, select_query, column_names):
    """
    Tranform a SELECT query into a pandas dataframe
    """
    cursor = conn.cursor()
    try:
        cursor.execute(select_query)
    except (Exception, psycopg2.DatabaseError) as error:
        print("Error: %s" % error)
        cursor.close()
        return 1

    # Naturally we get a list of tupples
    tupples = cursor.fetchall()
    cursor.close()

    # We just need to turn it into a pandas dataframe
    df = pd.DataFrame(tupples, columns=column_names)
    return df


print("import database")
conn = connect(param_dic)

column_names = ["leg_id",
                "participant_id",
                "legstart_timestamp",
                "legstart_lat",
                "legstart_long",
                "legend_timestamp",
                "legend_lat",
                "legend_long",
                ]

table_query = ' '.join(["SELECT",
                        "A.id AS leg_id,",
                        "F.participant_id,",
                        "B.departure_time AS legstart_timestamp,",
                        "st_x(st_astext(D.location, 4326)) as legstart_lat,",
                        "st_y(st_astext(D.location, 4326)) as legstart_long,",
                        "C.arrival_time AS legend_timestamp,",
                        "st_x(st_astext(E.location, 4326)) as legend_lat,",
                        "st_y(st_astext(E.location, 4326)) as legend_long",
                        "FROM uma_participant_trip_leg AS A",
                        "INNER JOIN uma_participant_stopover AS B ON A.from_stopover_id = B.id",
                        "INNER JOIN uma_participant_stopover AS C ON A.to_stopover_id = C.id",
                        "INNER JOIN uma_participant_place AS D ON B.place_id = D.id",
                        "INNER JOIN uma_participant_place AS E ON C.place_id = E.id",
                        "INNER JOIN uma_participant_trip AS F ON A.trip_id = F.id",
                        "WHERE F.participant_id IN",
                        "('3465a460-2d33-493c-99ba-2166b14ca898', '36e40431-212c-4323-bf58-9edb7a4a2813', ",
                        "'53714cf9-b990-476e-95a4-3201c113c6b9', '8711bc2f-4412-4bc2-9785-e8d6bed45ef7', ",
                        "'957f287b-2467-4ec0-b3bf-c9627b4b2230', '99fed303-77bb-417b-89b5-b70a87bf4a49', ",
                        "'abe17649-d025-45c9-96b8-a7468e8cf4a2', 'd4826376-8c0e-48b2-8e31-ad2edc44d1f4', ",
                        "'f5477b8e-26bb-4e21-9eb1-6d75cbcaa6ce', 'fa0ce243-b8e9-4267-a465-18d8902ca19c') ",
                        "limit 100"])

# create dataframe
gdf_legs = postgresql_to_dataframe(conn, table_query, column_names)

# prepare geometry data
legstart_long = gdf_legs['legstart_long']
legstart_lat = gdf_legs['legstart_lat']
legend_long = gdf_legs['legend_long']
legend_lat = gdf_legs['legend_lat']

legstart_points = gpd.points_from_xy(legstart_long, legstart_lat, crs='EPSG:4326')
legend_points = gpd.points_from_xy(legend_long, legend_lat, crs='EPSG:4326')

# create geodataframe
gdf_legs = gpd.GeoDataFrame(gdf_legs, geometry=legstart_points).rename(columns={'geometry': 'legstart_location'})
gdf_legs = gpd.GeoDataFrame(gdf_legs, geometry=legend_points).rename(columns={'geometry': 'legend_location'})

# assess geodataframe
print(gdf_legs.info())
print(gdf_legs.crs)
print(gdf_legs[["legstart_location",
                "legend_location"]
      ].head())

print("Import GeoJson")

file_path = os.path.join('..', '..', '..', '..', 'resources', 'data', 'external', 'places_spatial_data',
                         'latam_geojson', 'buenos-aires.geojson')

print(f"file path: {file_path}")
print(os.path.exists(file_path))

gdf_places = gpd.read_file(file_path)
print(gdf_places.info())
print(gdf_places.crs)
print(gdf_places['geometry'])

print("spatial join goejson to database")


def spatial_join(left_df, right_df, how, op, left_col=None, right_col=None):
    left_df = left_df.copy()
    right_df = right_df.copy()

    if left_col is not None:
        left_df = left_df.rename(columns={left_col: 'geometry'})

    if right_col is not None:
        right_df = right_df.rename(columns={right_col: 'geometry'})

    return gpd.sjoin(left_df, right_df, how=how, op=op)


gdf_legs_with_places = spatial_join(gdf_legs, gdf_places, how='inner', op='intersects', left_col='legstart_location')
gdf_legs_with_places = spatial_join(gdf_legs, gdf_places, how='inner', op='intersects', left_col='legend_location')

print(gdf_legs_with_places.info())
print(gdf_legs_with_places.crs)

aa = gdf_places.plot()
gdf_legs.rename(columns={'legstart_location': 'geometry'}).plot(ax=aa)
plt.show()
