class Trip:

    def __init__(self, participant_id, departure_time, arrival_time, legs):
        self.participant_id = participant_id
        self.departure_time = departure_time
        self.arrival_time = arrival_time
        self.legs = legs # this is a list of LegDtos

    def getTravelTime(self):
        return self.arrival_time - self.departure_time
