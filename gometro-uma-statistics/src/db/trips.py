from database import Database


class Trip:

    def __init__(self):
        self.trip_df = None

    def getTrips(self):
        database = Database()

        database.connect()

        table_query = "SELECT A.id AS leg_id, F.participant_id, A.polyline, B.departure_time AS legstart_timestamp, " \
                      "F.departure_time AS tripstart_timestamp, F.arrival_time AS tripend_timestamp" \
                      "st_x(st_astext(D.location, 4326)) as legstart_lat, " \
                      "st_y(st_astext(D.location, 4326)) as legstart_long, " \
                      "C.arrival_time AS legend_timestamp, " \
                      "st_x(st_astext(E.location, 4326)) as legend_lat, " \
                      "st_y(st_astext(E.location, 4326)) as legend_long " \
                      "FROM uma_participant_trip_leg AS A " \
                      "INNER JOIN uma_participant_stopover AS B ON A.from_stopover_id = B.id " \
                      "INNER JOIN uma_participant_stopover AS C ON A.to_stopover_id = C.id " \
                      "INNER JOIN uma_participant_place AS D ON B.place_id = D.id " \
                      "INNER JOIN uma_participant_place AS E ON C.place_id = E.id " \
                      "INNER JOIN uma_participant_trip AS F ON A.trip_id = F.id " \
                      "WHERE F.participant_id IN " \
                      "('3465a460-2d33-493c-99ba-2166b14ca898', '36e40431-212c-4323-bf58-9edb7a4a2813', " \
                      "'53714cf9-b990-476e-95a4-3201c113c6b9', '8711bc2f-4412-4bc2-9785-e8d6bed45ef7', " \
                      "'957f287b-2467-4ec0-b3bf-c9627b4b2230', '99fed303-77bb-417b-89b5-b70a87bf4a49', " \
                      "'abe17649-d025-45c9-96b8-a7468e8cf4a2', 'd4826376-8c0e-48b2-8e31-ad2edc44d1f4', " \
                      "'f5477b8e-26bb-4e21-9eb1-6d75cbcaa6ce', 'fa0ce243-b8e9-4267-a465-18d8902ca19c') " \
                      "limit 500"

        self.trip_df = database.query_to_df(table_query)

        return self.trip_df


#
# [
#         'participant' => '123-456-789'
# 'trips' => [
#         [
#                 'trip_id => '987-654-321''
#             'legs' => [
#         [
#
#
#         ]
#
# ]
#
#         ]
#
# ]
#     ]
# ]
#
#         return results

# df = Trip().getTrips()
# df['trip_duration'] = (df['arrival_time'] - df['departure_time']).dt.seconds
#
# print(df.info())
# print(df['trip_duration'].head())
