# @todo - Let's put this as a nice wrapper like we had for the drivers of the endpoints. Except this time we query!
# Only close the connection when the script is complete

import psycopg2
import pandas as pd


class Database:
    cursor = None

    def __init__(self):
        self.connect()

    def __del__(self):
        self.close()

    def connect(self):
        conn = psycopg2.connect(host='postgresql.gometroapp.com', database='gometro_uma',
                                user='gometro_uma', password='gometro_uma')
        self.cursor = conn.cursor()

    def query_to_df(self, statement):
        self.cursor.execute(statement)

        column_names = [desc[0] for desc in self.cursor.description];

        return self.postgresql_to_dataframe(self.cursor.fetchall(), column_names)

    def close(self):
        self.cursor.close()
        self.cursor = None

    def postgresql_to_dataframe(conn, tupples, column_names):
        # We just need to turn it into a pandas dataframe
        df = pd.DataFrame(tupples, columns=column_names)
        return df