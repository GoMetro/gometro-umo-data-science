import db/database;

db = database.connect()
trips = db.query_to_df(''
                       'select utp.departure_time, utp.arrival_time, utpl.from_stopover_id as departure_stopover, utpl.to_stopover_id as arrival_stopover'
                       'from uma_partip
                       '')

stopovers = db.query_to_df(''
                           'select ups.id, ups.departure_time, ups.arrival_time, upp.location'
                           'from uma_participant_stopover as ups'
                           'inner join uma_participant_place upp on ups.place_id = upp.id'
                           'where ups.id in (?)')

db.close()

# loop over stopover and hydrate stopover models
# new Place(stopovers.location)
# new Stopover(stopovers.place, stopovers.arrival_time, stopovers.departure_time)

# loop over trips and hydrate trip models
# new Trip(participantId, departureTime, arrivalTime, fromStopover, toStopover)

# loop over trip dtos and give complete output
# (sum tripDto.getTravelTime()) / tripDtos.length

# def reduce(tripDto, carry)
#    carry += tripDto.getTravelTime()

# create json of participant data
# e.g.
# { "average_travel_time": "3600" }

# write to _root_folder_/output/{todays_date}/{participant_id}.json