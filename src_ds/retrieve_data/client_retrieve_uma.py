"""UMA API Data Retrieval"""

from uma.uma_api_response import UmaApiAuthenticator
from uma.uma_trip_retriever import TripRetriever
from uma.uma_events_retriever import DeviceEventsRetriever
from os.path import join

auth_file_path = join('..', '..', '..', '..', 'Admin', 'authentication_files',
                      'uma_api_cred_2021-01-21.json')

participant_id = 'e2dc773b-15e0-4451-8c09-faf5ed644699'
trip_date = '2021-01-06'
trips_start_date = '2021-01-06'
trips_end_date = '2021-01-21'

uma_auth = UmaApiAuthenticator()\
    .set_credentials(auth_file_path)


# getting trips: range of dates for participant
uma_trips_retriever_select = TripRetriever() \
    .set_participant_id(participant_id) \
    .set_trip_params(start_date=trips_start_date, end_date=trips_end_date) \
    .fetch_request()

# getting trips: all trips for participant
uma_trips_retriever_all = TripRetriever() \
    .set_participant_id(participant_id) \
    .fetch_request()

print(uma_trips_retriever_all.uma_url)
print(uma_trips_retriever_select.uma_url)

# getting events: single date
uma_events_retriever_single = DeviceEventsRetriever() \
    .set_participant_id(participant_id) \
    .set_event_date(trip_date) \
    .fetch_request()
#
print(uma_events_retriever_single.uma_url)

# # getting events: list of dates
# uma_events_retriever_list = DeviceEventsRetriever()\
# 						.set_participant_id(participant_id)\
# 						.set_event_date([trips_start_date, trip_date, trips_end_date])


trips_table_all = uma_trips_retriever_all.create_response_table(uma_auth)
trips_table_select = uma_trips_retriever_select.create_response_table(uma_auth)
events_table_single = uma_events_retriever_single.create_response_table(uma_auth)

# events_table_list = uma_events_retriever_list.create_response_table(uma_auth)


print(trips_table_all.info())
print(trips_table_select.info())
print(events_table_single.info())
# print(events_table_list)
