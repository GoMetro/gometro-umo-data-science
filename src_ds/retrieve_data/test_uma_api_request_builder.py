import unittest
from uma.uma_api_request_builder import UmaApiRequestBuilder
from uma.uma_api_response import UmaApiResponse


class TestUmaApiRequestBuilder(unittest.TestCase):

    def setUp(self):
        self.events1 = UmaApiRequestBuilder()

        self.events_setUp1 = UmaApiRequestBuilder()
        self.events_setUp2 = UmaApiRequestBuilder()
        self.events_setUp3 = UmaApiRequestBuilder()

        self.trips1 = UmaApiRequestBuilder()
        self.trips2 = UmaApiRequestBuilder()
        self.trips3 = UmaApiRequestBuilder()
        self.trips4 = UmaApiRequestBuilder()

        self.trips_setUp1 = UmaApiRequestBuilder()
        self.trips_setUp2 = UmaApiRequestBuilder()
        self.trips_setUp3 = UmaApiRequestBuilder()
        self.trips_setUp4 = UmaApiRequestBuilder()

        self.events_setUp1 \
            .set_participant_id('events1-participant-id') \
            .set_endpoint_type('events') \
            .set_event_date('2021-01-01')

        self.events_setUp2 \
            .set_participant_id('events1-participant-id')

        self.events_setUp3 \
            .set_endpoint_type('events')

        self.trips_setUp1 \
            .set_participant_id('trips1-participant-id') \
            .set_endpoint_type('trips')

        self.trips_setUp2 \
            .set_participant_id('trips2-participant-id') \
            .set_endpoint_type('trips') \
            .set_trip_params(start_date='2021-01-01', end_date='2021-01-25')

        self.trips_setUp3 \
            .set_participant_id('trips3-participant-id') \
            .set_endpoint_type('trips') \
            .set_trip_params(start_date='2021-01-01')

        self.trips_setUp4 \
            .set_participant_id('trips4-participant-id') \
            .set_endpoint_type('trips') \
            .set_trip_params(end_date='2021-01-25')

    def tearDown(self):
        print('tearDown\n')

    def test_set_participant_id(self):
        self.events1.set_participant_id('events1-participant-id')
        self.trips1.set_participant_id('trips1-participant-id')

        self.assertEqual(self.events1.uma_api_response.participant_id, 'events1-participant-id')
        self.assertEqual(self.trips1.uma_api_response.participant_id, 'trips1-participant-id')

    def test_set_endpoint_type(self):
        self.events1.set_endpoint_type('events')
        self.trips1.set_endpoint_type('trips')

        self.assertEqual(self.events1.uma_api_response.endpoint_type, 'events')
        self.assertEqual(self.trips1.uma_api_response.endpoint_type, 'trips')

    def test_set_event_date(self):
        self.events1.set_event_date('2021-01-01')

        self.assertEqual(self.events1.uma_api_response.event_date, '2021-01-01')

    def test_set_trip_params(self):
        self.trips2.set_trip_params(start_date='2021-01-01', end_date='2021-01-25')
        self.trips3.set_trip_params(start_date='2021-01-01')
        self.trips4.set_trip_params(end_date='2021-01-25')

        self.assertEqual(self.trips2.uma_api_response.trip_params,
                         {'fromDate': '2021-01-01', 'toDate': '2021-01-25'})

        self.assertEqual(self.trips3.uma_api_response.trip_params,
                         {'fromDate': '2021-01-01'})

        self.assertEqual(self.trips4.uma_api_response.trip_params,
                         {'toDate': '2021-01-25'})

    def test_get_url_path(self):
        # check for when paricipant_id

        # Arrange
        # setUp

        # Act
        self.events_setUp1.get_url_path()
        self.trips_setUp1.get_url_path()
        self.trips_setUp2.get_url_path()
        self.trips_setUp3.get_url_path()
        self.trips_setUp4.get_url_path()

        # Assert
        self.assertEqual(self.events_setUp1.uma_api_response.url_path,
                         '/uma/v1/participants/events1-participant-id/events/2021-01-01')

        self.assertEqual(self.trips_setUp1.uma_api_response.url_path,
                         '/uma/v1/participants/trips1-participant-id/trips')
        self.assertEqual(self.trips_setUp2.uma_api_response.url_path,
                         '/uma/v1/participants/trips2-participant-id/trips')
        self.assertEqual(self.trips_setUp3.uma_api_response.url_path,
                         '/uma/v1/participants/trips3-participant-id/trips')
        self.assertEqual(self.trips_setUp4.uma_api_response.url_path,
                         '/uma/v1/participants/trips4-participant-id/trips')

    def test_get_url_path_exceptions(self):
        self.assertRaises(TypeError, self.events_setUp2.get_url_path)  # no participant id
        self.assertRaises(TypeError, self.events_setUp3.get_url_path)  # no event type

    def test_get_url_query(self):
        self.trips_setUp2.get_url_query()
        self.trips_setUp3.get_url_query()
        self.trips_setUp4.get_url_query()

        self.assertEqual(self.trips_setUp2.uma_api_response.url_query,
                         'fromDate=2021-01-01&toDate=2021-01-25')
        self.assertEqual(self.trips_setUp3.uma_api_response.url_query,
                         'fromDate=2021-01-01')
        self.assertEqual(self.trips_setUp4.uma_api_response.url_query,
                         'toDate=2021-01-25')

    def test_get_uma_url(self):
        self.events_setUp1.get_url_path()
        self.trips_setUp1.get_url_path()
        self.trips_setUp2.get_url_path()
        self.trips_setUp3.get_url_path()
        self.trips_setUp4.get_url_path()

        self.trips_setUp2.get_url_query()
        self.trips_setUp3.get_url_query()
        self.trips_setUp4.get_url_query()

        self.events_setUp1.get_uma_url()
        self.trips_setUp1.get_uma_url()
        self.trips_setUp2.get_uma_url()
        self.trips_setUp3.get_uma_url()
        self.trips_setUp4.get_uma_url()

        self.assertEqual(self.events_setUp1.uma_api_response.uma_url,
                         "https://api.gometroapp.com/uma/v1/participants/events1-participant-id/events/2021-01-01")

        self.assertEqual(self.trips_setUp1.uma_api_response.uma_url,
                         "https://api.gometroapp.com/uma/v1/participants/trips1-participant-id/trips")

        self.assertEqual(self.trips_setUp2.uma_api_response.uma_url,
                         "https://api.gometroapp.com/uma/v1/participants/trips2-participant-id/trips?fromDate=2021"
                         "-01-01&toDate=2021-01-25")
        self.assertEqual(self.trips_setUp3.uma_api_response.uma_url,
                         "https://api.gometroapp.com/uma/v1/participants/trips3-participant-id/trips?fromDate=2021"
                         "-01-01")
        self.assertEqual(self.trips_setUp4.uma_api_response.uma_url,
                         "https://api.gometroapp.com/uma/v1/participants/trips4-participant-id/trips?toDate=2021-01"
                         "-25")

    def test_get_url_exceptions(self):
        self.assertRaises(TypeError, self.events_setUp1.get_uma_url)  # no url path

    def test_get_result(self):
        self.assertIsInstance(self.events1.get_result(), UmaApiResponse)
        self.assertIsInstance(self.trips1.get_result(), UmaApiResponse)


if __name__ == '__main__':
    unittest.main()
