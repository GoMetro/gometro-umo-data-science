"""A Director Class"""
from .uma_api_request_builder import UmaApiRequestBuilder


class TripRetriever(UmaApiRequestBuilder):  # pylint: disable=too-few-public-methods
    """Objects of this class retrieve trip data from the UMA API."""

    def fetch_request(self):
        """Constructs url request for UMA API"""

        self.set_endpoint_type('trips')
        self.get_url_path()

        try:
            self.get_url_query()
        # if trip_params not set
        except AttributeError:
            pass
        except TypeError:
            pass

        self.get_uma_url()

        return self.get_result()


