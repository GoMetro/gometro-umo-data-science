"""The Builder Class"""
from .interface_uma_api_request_builder import IUmaApiRequestBuilder
from .uma_api_response import UmaApiResponse
from urllib import parse


class UmaApiRequestBuilder(IUmaApiRequestBuilder):
    """The House Builder."""

    def __init__(self):
        self.uma_api_response = UmaApiResponse()

    def set_participant_id(self, participant_id):
        self.uma_api_response.participant_id = participant_id
        return self

    def set_endpoint_type(self, uma_endpoint_type):
        self.uma_api_response.endpoint_type = uma_endpoint_type
        return self

    def set_event_date(self, event_date):

        self.uma_api_response.event_date = event_date

        return self

    def set_trip_params(self, start_date=None, end_date=None):

        self.uma_api_response.trip_params = {}

        if start_date is not None:
            self.uma_api_response.trip_params['fromDate'] = start_date

        if end_date is not None:
            self.uma_api_response.trip_params['toDate'] = end_date

        return self

    def get_url_path(self):

        url_path = '/uma/v1/participants'

        if self.uma_api_response.participant_id is not None:
            url_path = '/'.join([url_path, self.uma_api_response.participant_id])

        else:
            raise TypeError("participant_id not set. use set_participant_id first, then use get_url_path")

        if self.uma_api_response.endpoint_type is not None:
            url_path = '/'.join([url_path, self.uma_api_response.endpoint_type])

        else:
            raise TypeError("endpoint_type not set. use set_endpoint first, then use get_url_path")

        if self.uma_api_response.event_date is not None:
            url_path = '/'.join([url_path, self.uma_api_response.event_date])

        self.uma_api_response.url_path = url_path

        return self.uma_api_response.url_path

    def get_url_query(self):

        self.uma_api_response.url_query = parse.urlencode(self.uma_api_response.trip_params)

    def get_uma_url(self):

        uma_url = "https://api.gometroapp.com/uma/v1"

        uma_url_segments = parse.urlparse(uma_url)

        if self.uma_api_response.url_path is not None:
            uma_url_segments = uma_url_segments._replace(path=self.uma_api_response.url_path)

        else:
            raise TypeError("url_path not set. use get_url_path first, then use get_uma_url")

        if self.uma_api_response.url_query is not None:
            uma_url_segments = uma_url_segments._replace(query=self.uma_api_response.url_query)

        self.uma_api_response.uma_url = parse.urlunparse(uma_url_segments)

        return self.uma_api_response.uma_url

    def get_result(self):

        return self.uma_api_response
