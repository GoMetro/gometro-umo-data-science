"The Builder Interface"
from abc import ABCMeta, abstractmethod


class IUmaApiRequestBuilder(metaclass=ABCMeta):
    "The House Builder Interface"

    @staticmethod
    @abstractmethod
    def set_participant_id(participant_id):
        "participant id"

    @staticmethod
    @abstractmethod
    def set_endpoint_type(uma_endpoint_type):
        "uma endpoint type. example: trips "

    @staticmethod
    @abstractmethod
    def set_event_date(date):
        "uma event date. Unicode format"

    @staticmethod
    @abstractmethod
    def set_trip_params(start_date, end_date):
        "date range parameters for getting trips from uma api"

    @staticmethod
    @abstractmethod
    def get_url_path(self):
        "create url segment: /uma/v1/{participantId}/{endpoint_type}[{event_date}]"

    @staticmethod
    @abstractmethod
    def get_url_query(self):
        "create url segment: fromDate={start_date}&toDate={end_date}"

    @staticmethod
    @abstractmethod
    def get_uma_url(self):
        "create final url: https://api.gometroapp.com{path}?{query}"

    @staticmethod
    @abstractmethod
    def get_result():
        "Return the final product"
