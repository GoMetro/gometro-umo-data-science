"The Product"

# analysis
import numpy as np
import pandas as pd
import json

# other
# import warnings
import os
import requests


def raise_uma_response_exceptions(response):
    """
    This function raises exceptions for responses from the
    UMA API
    
    -----------------
    Example:
    
    """

    # raise object type error
    if type(response) is not requests.models.Response:
        raise TypeError("Expected type response.models.Response. instead passed {type(response)}")

    # raise bad response error
    if response.status_code != 200:
        raise ValueError(f'Response Error. {response.status_code}')

    # raise empty response error
    assert len(response.json()) > 0, "response returned no data"

    # raise response timeout error
    if response.json() == {'message': 'Endpoint request timed out'}:
        raise TimeoutError(response.json()['message'])


class UmaApiAuthenticator():
    """
    An UMA token object is designed to store UMA authentication
    credenitals and generate an UMA API authentication token

    ------------
    Example:

    >>> from os.path import join
    >>> auth_file_path = os.path.join('path','to','authentication','credentials'.'file.json')
    >>> uma_auth = UmaApiAuthenticator(auth_file_path)
    >>> uma_auth.generate_token()
    >>> uma_api_token = uma_auth.token
      
    """

    endpoint = 'https://identity.gometroapp.com/auth/realms/platform/protocol/openid-connect/token'

    def __init__(self, credentials=None, response=None, token=None):
        self.credentials = credentials
        self.response = response
        self.token = token

    def set_credentials(self, credentials_file_path):
        with open(credentials_file_path, "r") as read_file:
            self.credentials = json.load(read_file)

        return self

    def fetch_request(self, endpoint):
        self.response = requests.post(endpoint, data=self.credentials)
        raise_uma_response_exceptions(self.response)

        return self

    def generate_token(self):
        self.fetch_request(UmaApiAuthenticator.endpoint)
        self.token = self.response.json()["access_token"]

        return self


class UmaApiResponse():
    """
    An UMA events object is designed to store request data for
    calls made to the UMA API and retrieve the associated
    response data

    ------------
    Example:
    >>> endpoint = 
    >>> events_retriever = retrive_uma_events(endpoint)
    
    """

    def __init__(self, participant_id=None, endpoint_type=None,
                 event_date=None, trip_params=None, url_path=None,
                 url_query=None, uma_url=None, response=None, response_table=None):

        self.participant_id = participant_id
        self.endpoint_type = endpoint_type
        self.event_date = event_date
        self.trip_params = trip_params
        self.url_path = url_path
        self.url_query = url_query
        self.uma_url = uma_url
        self.response = response
        self.response_table = response_table

    def fetch_request(self, api_token):
        headers = {'Authorization': f'Bearer {api_token}'}

        self.response = requests.get(self.uma_url, headers=headers)

        raise_uma_response_exceptions(self.response)

        return self.response

    def create_response_table(self, uma_auth):
        uma_auth.generate_token()

        self.fetch_request(uma_auth.token)
        self.response_table = pd.DataFrame(self.response.json())

        return self.response_table
