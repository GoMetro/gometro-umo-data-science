"A Director Class"

from .uma_api_request_builder import UmaApiRequestBuilder


class DeviceEventsRetriever(UmaApiRequestBuilder):  # pylint: disable=too-few-public-methods
    "Objects of this class retrieve trip data from the UMA API."

    def fetch_request(self):
        "Constructs url request for UMA API"

        self.set_endpoint_type('events')
        self.get_url_path()
        self.get_uma_url()

        return self.get_result()


